module.exports = {
  dashboard: 'Panel',
  backButon: 'atras',
  nextButond: 'siguiente',
  plusButton: 'mas',
  home: 'Inicio',
  minusButton: 'menos',
  homeButton: 'Inicio',
  logIn: 'Iniciar sesion',
  logOut: 'Cerrar sesion',
  date: 'fecha',
  from: 'desde',
  articles: 'Articulos',
  article: 'Articulo',
  shoes: 'Zapatos',
  shoe: 'zapato',
  cuantity: 'cantidad',
  amount: 'Monto',
  price: 'Precio',
  createAt: 'Fecha de creacion',
  updatedAt: 'Fecha de actualizacion',
  id: 'id',
  brand: 'marca',
  size: 'tamaño',
  color: 'color',
  upName: 'Nombre',
  doName: 'name',
  more: 'Mas'
}
