import React, { Component } from 'react'
import { Container } from 'unstated'

import jwt from 'jsonwebtoken'
const secretKey = process.env.SECRET_KEY_TOKEN || 'L7ILc68aKL7ILcgzMk9XattMlR'

export default class MainContainer extends Container {
  state = {
    token: '',
    contacts: [],
    account_id: '',
    account: {}
    //morethings
  };

  resolveJwt = async (o) => {
    return await jwt.verify(o, `${secretKey}`)
  }

  setToken = async (t) => {
    this.setState({ token: t })
    let r = await resolveJwt(t)
    let { id } = r
    await this.setState({ account_id: id })
  }

  setAccount = async (a) => {
    await this.setState({ account: a })
  }

  setContacts = async (contacts) => {
    await this.setState(prevState => ({
        contacts: [...prevState.contacts, contacts]
      })
    )
    //with this form of setState we just add, not remove and adds
  }
  authenticate = async () => {
    
  }
  doLogout = async () => {
    // let result = await logout({
    //   email: this.state.account.email,
    //   token: this.state.token
    // })

    if (result.data.action === true) {
      await this.setState({ token: '.' })
      await this.setState({ account: '.' })
      await this.setState({ account_id: '.' })
      await this.setState({ contacts: '.' })
      return true
    }
    return false
  }
}
//this is the container
