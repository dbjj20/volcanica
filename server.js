const cors = require('micro-cors')()
const express = require('express')
const next = require('next')
const {router, get} = require('microrouter')

const dev = process.env.NODE_ENV
const app = next({ dev })
const handle = app.getRequestHandler()
const appPort = process.env.APP_PORT || 3000

app.prepare()

function hello (req, res) {
  return handle(req, res)
}
function mod (req, res){
  return app.render(req, res, '/mod', { slug: req.params.slug })
}

function dashboard (req, res){
  return app.render(req, res, '/dashboard', { slug: req.params.slug })
}
function registration (req, res){
  return app.render(req, res, '/registration', { slug: req.params.slug })
}
function settings (req, res){
  return app.render(req, res, '/settings', { slug: req.params.slug })
}

function login (req, res){
  //xyz = hash provided from backend
  return app.render(req, res, '/login', { xyz: req.params.xyz })
}
function article (req, res){
  //xyz = hash provided from backend
  return app.render(req, res, '/articles/articleDetail', { itemId: req.params.itemId })
}
//test page
// server.get('/post/:slug', (req, res) => {
//   return app.render(req, res, '/post', { slug: req.params.slug })
// })
//end

const routes = cors(router(
    get('/mod/:slug', mod),
    get('/registration/:slug', registration),
    get('/dashboard/:slug', dashboard),
    get('/settings/:slug', settings),
    get('/login/:xyz', login),
    get('/articles/articleDetail/:itemId', login),
    get('*', hello)
  ))

if (process.env.NODE_ENV === 'production') {
  module.exports = routes

} else {
  // developmetn mode with micro is hell, micro-dev does not work
  const server = express()

  server.get('*', (req, res) => {
    return handle(req, res)
  })
  server.get('/mod/:slug', (req, res) => {
    return app.render(req, res, '/mod', { slug: req.params.slug })
  })

  server.get('/registration/:slug', (req, res) => {
    return app.render(req, res, '/registration', { slug: req.params.slug })
  })

  server.get('/dashboard/:slug', (req, res) => {
    return app.render(req, res, '/dashboard', { slug: req.params.slug })
  })

  server.get('/settings/:slug', (req, res) => {
    return app.render(req, res, '/settings', { slug: req.params.slug })
  })
  server.get('/login/:xyz', (req, res) => {
    return app.render(req, res, '/login', { xyz: req.params.xyz })
  })
  server.get('/articles/articleDetail/:itemId', (req, res) => {
    return app.render(req, res, '/article', { item: req.params.itemId })
  })

  server.listen(appPort, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${appPort}`)
  })
}
