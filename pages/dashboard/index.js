import React, {Component} from 'react'
import { Subscribe, Provider } from 'unstated'
import MainContainer from '../../lib/storage'

export default class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      account: {},
      xyz: ''
    }
  }
  // async componentDidMount () {
  //   location.search.substr(1).split('&').forEach(pair => {
  //    this.setState({xyz: pair.split('=')[1]})
  //   })
  // }

  render(){
    return (
      <div>
        <Provider>
          <Subscribe to={[MainContainer]}>{
            state => <h1>hello</h1>
          }</Subscribe>
        </Provider>
      </div>
    )
  }
}
