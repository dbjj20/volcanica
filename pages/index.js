import Articles from './articles'
function HomePage({language}) {
  return (
    <div className="">
      <header className="masthead" style={{backgroundColor: "#0000", backgroundImage: `url(http://localhost:3000/static/img/eso.jpg)`}}>
        <div className="container default-color-text">
          <div className="intro-text">
            <div className="intro-lead-in">Volcania!</div>
            <div className="intro-heading text-uppercase">Lugar para las Rosas</div>
            <a className="button is-warning btn-xl text-uppercase js-scroll-trigger" href="/articles">Productos</a>
          </div>
        </div>
      </header>
      {/*Portafolio*/}
      <section className="block">
        <div className="container">

          <div className="columns center-text">
            <div className="column">
              <div className="intro-lead-in">Productos</div>
              <h3 className="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
          </div>

          {/* First part */}

          <div className="columns">
            <div className="column portfolio-item">
              <a className="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content">
                    <i className="fa fa-plus fa-3x"></i>
                  </div>
                </div>
                <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
              </a>
              <div className="portfolio-caption">
                <h4>Threads</h4>
                <p className="text-muted">Illustration</p>
              </div>
            </div>
            <div className="column portfolio-item">
              <a className="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content">
                    <i className="fa fa-plus fa-3x"></i>
                  </div>
                </div>
                <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
              </a>
              <div className="portfolio-caption">
                <h4>Explore</h4>
                <p className="text-muted">Graphic Design</p>
              </div>
            </div>
            <div className="column portfolio-item">
              <a className="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                <div className="portfolio-hover">
                  <div className="portfolio-hover-content">
                    <i className="fa fa-plus fa-3x"></i>
                  </div>
                </div>
                <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
              </a>
              <div className="portfolio-caption">
                <h4>Finish</h4>
                <p className="text-muted">Identity</p>
              </div>
            </div>
          </div>
            {/* Second part */}

            <div className="columns">
              <div className="column portfolio-item">
                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                  <div className="portfolio-hover">
                    <div className="portfolio-hover-content">
                      <i className="fa fa-plus fa-3x"></i>
                    </div>
                  </div>
                  <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
                </a>
                <div className="portfolio-caption">
                  <h4>Suculentas</h4>
                  <p className="text-muted">Hermosas en el Hogar</p>
                </div>
              </div>
              <div className="column portfolio-item">
                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                  <div className="portfolio-hover">
                    <div className="portfolio-hover-content">
                      <i className="fa fa-plus fa-3x"></i>
                    </div>
                  </div>
                  <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
                </a>
                <div className="portfolio-caption">
                  <h4>Zapatos</h4>
                  <p className="text-muted">Calzan a la perfeccion</p>
                </div>
              </div>
              <div className="column portfolio-item">
                <a className="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                  <div className="portfolio-hover">
                    <div className="portfolio-hover-content">
                      <i className="fa fa-plus fa-3x"></i>
                    </div>
                  </div>
                  <img className="img-fluid" src="http://localhost:3000/static/img/sucu.jpeg" alt=""/>
                </a>
                <div className="portfolio-caption">
                  <h4>Bolsos</h4>
                  <p className="text-muted">Combina con el que mas te guste</p>
                </div>
              </div>
            </div>
          </div>
      </section>
    </div>
  )
}

export default HomePage
