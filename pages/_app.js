import '../public/static/style.css'
import 'bulma/css/bulma.css'
import React from 'react'
import setLanguage from '../lib/languages'
import 'isomorphic-unfetch'

// import all required global styles if required
// but them into /public/static/ for standarization
export default function MyApp({ Component, pageProps }) {
  // language must be set by cookies or settings from server
  let language = setLanguage('spanish')
  return (
    <div>
    {/*==== NAV BAR====*/}
      <div>
        <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
            <a className="navbar-item" href="/">
              <h2 style={{fontSize: "37px", paddingLeft: "10px", color:"rgb(255, 209, 3)"}}>Volcania</h2>
            </a>

            <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

          <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-start">

              <a className="navbar-item" href="/articles" style={{color:"rgb(255, 209, 3)"}}>
                {language.articles}
              </a>

              <a href="/dashboard" className="navbar-item" style={{color:"rgb(255, 209, 3)"}}>
                {language.dashboard}
              </a>

              <div className="navbar-item has-dropdown is-hoverable" >
                <a className="navbar-link" style={{color:"rgb(255, 209, 3)"}}>
                  {language.more}
                </a>

                <div className="navbar-dropdown">
                  <a className="navbar-item">
                    Zapatos
                  </a>
                  <a className="navbar-item">
                    Maquillajes
                  </a>
                  <a className="navbar-item">
                    Bolsos
                  </a>
                  <hr className="navbar-divider"/>
                  <a className="navbar-item">
                    Ropa
                  </a>
                </div>
              </div>
            </div>

            <div className="navbar-end">
              <div className="navbar-item">
                <div className="buttons">
                  <a className="button is-primary">
                    <strong>Sign up</strong>
                  </a>
                  <a href="/login" className="button is-light">
                    Log in
                  </a>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
      {/*==== NAV BAR====*/}

        {/*==== MAIN COMP====*/}


            <div className="" >
                <Component {...pageProps} language={language} />
            </div>

        {/*==== MAIN COMP====*/}

      <footer className="custom-footer">
        <div className="has-text-centered">
          <a href="/">
            <img src="static/img/facebook.svg" />
          </a>
          <br/>
          <a href="/">
            <img src="static/img/instagram.svg" />
          </a>
          <p>
            <strong>Volcania</strong> by <a href="https://www.linkedin.com/in/dylan-baez-jimenez-a6494ba6/">xDstriker</a>
          </p>
          <p>
          <strong>Nosotros</strong>
          </p>
          <p>
          <strong>Contactanos</strong>
          </p>
          <p>
          <strong>Envios</strong>
          </p>
        </div>
      </footer>
    </div>
  )
}
