import React, {Component} from 'react'
import {Provider} from 'unstated'
import MainContainer from '../../lib/storage'
import Login from './login'

class HomeLogin extends Component{
  constructor(props) {
    super(props)
  }
  render(){
    return (
      <Provider>
        <Login language={this.props.language}/>
      </Provider>
    )
  }
}

export default HomeLogin
