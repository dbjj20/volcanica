import React, {Component} from 'react'

export default class ChildLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      account: {},
      xyz: ''
    }
    this.handleLogin = this.handleLogin.bind(this)
  }

  async handleLogin() {
    debugger
    let result = await this.props.superState.authenticate(state)
    console.log(result)
    if(result.data.auth === false) {
      alert('Bad user or password')
    }

    if (result.data.token) {
      //set the cookies for automate login and more things in the future
      if (result.data.cookie) {
        let s = new Date().getTime() + 86400000 * 3
        document.cookie = `uid=${result.data.cookie}; path=/; expires=${new Date(s)}`
      }

      this.setState({ logedIn: true })
      // await this.props.superState.setToken(result.data.token)
      // await this.props.superState.setAccount(result.data.account)
      //here we are setting the token to the MainContainer state to use it in the entire app
      await this.props.login('logedIn', true)
      //here we see login is inheriting handleLogin from the home login method
      // debugger
    }
  }
  // async componentDidMount () {
  //   location.search.substr(1).split('&').forEach(pair => {
  //    this.setState({xyz: pair.split('=')[1]})
  //   })
  // }

  render(){
    return (
      <div>
      <div className="container animate" style={{width: '600px'}}>
        <div className="container">
          <label htmlFor="uname"><b>Username</b></label>
          <input className="customInput" type="text"  name="email"  placeholder='Your Email' required/>

          <label htmlFor="psw"><b>Password</b></label>
          <input className="customInput" name="password" type="password"  placeholder="Your Password"  securetextentry="true" required/>

          <button className="customButton" type="button"  >Login</button>
          <label>
            <br></br>
          </label>
        </div>

        <div className="container" style={{backgroundColor:"#f1f1f1"}}>
          <button hidden className="customButton" type="button"  className="cancelbtn">Cancel</button>
          <span hidden className="psw">Forgot <a href="#">password?</a></span>
        </div>
      </div>
      </div>
    )
  }
}
