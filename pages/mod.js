import { useRouter } from 'next/router'

function HomePage() {
  const router = useRouter()
  const { slug } = router.query
  
  return (<div>
    <a href="/">Home</a>
    <h1>klk :D {slug}</h1>
    </div>)
}

export default HomePage
