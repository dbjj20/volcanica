import { useRouter } from 'next/router'
import Article from '../../components/article'
function Articles() {
  const router = useRouter()
  const { slug } = router.query

  return (
    <div>
        <div className="container">
        <br/>
        <div className="">
          <div className="columns">
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>

          </div>

        </div>
        <div className="">
          <div className="columns">
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
          </div>

        </div>
        <div className="">
          <div className="columns">
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
          </div>

        </div>
        <div className="">
          <div className="columns">
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
            <div className="column is-2"><Article /></div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Articles
