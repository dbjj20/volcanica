import React, {Component} from 'react'
import { Slide } from 'react-slideshow-image';

let slideIndex = 1;

const slideImages = [
  'img/papa.png',
  'img/Cuidar-tus-cactus-y-suculentas.jpg',
  'img/UY500.jpg'
];

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
  onChange: (oldIndex, newIndex) => {
    // console.log(`slide transition from ${oldIndex} to ${newIndex}`);
  }
}

export default class ArticleDetail extends Component{
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount () {
    //use location.serach to get query params
  }

    render() {
      return (
        <div className="slide-container">
        <Slide {...properties}>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(http://localhost:3000/static/${slideImages[0]})`}}></div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(http://localhost:3000/static/${slideImages[1]})`}}></div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(http://localhost:3000/static/${slideImages[2]})`}}></div>
          </div>
        </Slide>
      </div>
      )
    }
}
