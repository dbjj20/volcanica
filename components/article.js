export default function Article ({item}){
  return (
    <div className="portfolio-item">
      <a className="portfolio-link" href="/articles/articleDetail?itemId=123123">
        <div className="portfolio-hover">
          <div className="portfolio-hover-content">
            <i className="fa fa-plus fa-3x"></i>
          </div>
        </div>
        <img className="img-fluid" src="static/img/dylitan.png" alt=""/>
      </a>
      <div className="portfolio-caption">
        <h4>nombre del articulo</h4>
        <p className="text-muted">mensaje del articulo</p>
      </div>
    </div>
  )
}
